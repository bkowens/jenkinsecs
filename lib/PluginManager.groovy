import jenkins.model.*
class PluginManager {
    def pluginParameter
    def instance = Jenkins.getInstance()
    def pm = instance.getPluginManager()

    public PluginManager(pluginParameter="default"){
        this.pluginParameter = pluginParameter
    }

    public def checkInstalled(pluginList) {
        def list = []
        list << "Git"
        pluginList.split().each {
            def installed = false
            if (installed) {
              list << it
            }


        }
        return list
    }

    public def isInstalled(singlePlugin) {
        if (!pm.getPlugin(singlePlugin)) {
            return "Not installed"
        } else {
            return "It is installed"
        }
    }

    public def listPlugins() {
      def plugins = []
      pm.plugins.each {
         plugins << it.getShortName()
      }
      return plugins
    }

    public def installPlugin(pluginShortnames) {
        def installed = false
        pluginShortnames.split().each {
            if (!instance.getPlugin(it)) {
                def plugin = instance.getUpdateCenter().getPlugin(it)
                if (plugin) {
                    println("Installing " + it)
                    plugin.deploy()
                    installed = true
                }
            }
        }
        instance.save()
        if (installed) {
            instance.doSafeRestart()
        }
        return "Rebooting."
    }
}
